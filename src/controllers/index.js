/**
 * Created by hendy on 24/03/16.
 */
import express from 'express'
const router = express.Router()
import _ from 'lodash'

router.post('/', (req, res) => {
  let { payload } = req.body
  let response = []

  _.forEach(payload, (property) => {
    let { address, type, workflow } = property
    let { buildingNumber, postcode, state, suburb, street } = address
    let concataddress = `${buildingNumber} ${street} ${suburb} ${state} ${postcode}`
    response.push({ concataddress, type, workflow })
  })

  res.json({
    response
  })
})

export default router

