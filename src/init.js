import Express from 'express'
import middlewares from './middlewares'
import applyMiddlewares from './helpers/applyMiddelwares'
import controllers from './controllers'

function init (port, callback) {
  if (!port) {
    throw new Error('Invalid port number:' + port)
  }

  // base server
  let server = new Express()

  // app that runs in v1
  let app = Express()

  applyMiddlewares(app, middlewares)
  app.use(controllers)

  server.use('/', app)
  server.all('*', function (req, res) {
    res.status(404).json({
      error: 'Not found'
    })
  })

  server = server.listen(port, () => {
    callback(null, server)
  })
}

export default init
