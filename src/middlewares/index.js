/**
 * Created by hendy on 24/03/16.
 */

import morgan from 'morgan'
import cors from 'cors'
import bodyParser from 'body-parser'

const middlewares = [
  morgan('combined'),
  cors({
    credentials: true,
    origin: /.*/
  }),
  bodyParser.json(),
  // custom handler for invalid JSON
  function (error, req, res, next) {
    if (error instanceof SyntaxError) {
      res.status(400).json({
        error: 'Could not decode request: JSON parsing failed'
      })
    } else {
      next()
    }
  }
]

export default middlewares
