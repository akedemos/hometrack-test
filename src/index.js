import init from './init'

let {PORT} = process.env

init(PORT, () => {
  console.log('server started on port:' + PORT)
})
